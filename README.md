Translations repository created by Weblate
==========================================

See https://weblate.org/ for more info.


This repository holds the Glossary for Tor Project in Weblate.

Terms can be edited and translated at 
https://hosted.weblate.org/projects/tor/glossary/

This glossary is not the one at https://support.torproject.org/glossary/
although most of those terms should also appear here.
This glossary also has tips for translators, common Internet-related slang
and acronyms, and others.

Please contribute to this glossary in your language, to have more consistent
translations and help new translators to understand the context better.
